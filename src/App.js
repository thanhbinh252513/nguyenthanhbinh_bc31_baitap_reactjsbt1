import { Component } from "react";
import "./App.css";
// import BodyBanner from "./BodyBanner/BodyBanner";
// import BodyItem from "./BodyItem/BodyItem";

import Glasses from "./Component/Glasses";
import ItemGlass from "./Component/ItemGlass";
import Model from "./Component/Model";
// import Footer from "./Footer/Footer";
import Header from "./Header/Header";

function App() {
  return (
    <div className="App">
      {/* <Header /> */}
      {/* <BodyBanner />
      <BodyItem />
      <Footer /> */}
      {/* <Com /> */}

      <Component />
    </div>
  );
}

export default App;
