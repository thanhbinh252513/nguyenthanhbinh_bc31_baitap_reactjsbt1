import React, { Component } from "react";

export default class ItemGlass extends Component {
  render() {
    let { id, price, url, name, image, desc } = this.props.glasses;
    return (
      <>
        <img
          onClick={() => {
            this.props.handleChangeGlass(this.props.glasses);
          }}
          className=""
          src={image}
          alt={name}
          style={{
            cursor: "pointer",
            width: "150px",
            height: "100px",
            objectFit: "contain",
          }}
        />
      </>
    );
  }
}
