import React, { Component } from "react";
import ChildBodyItem from "./ChildBodyItem";
export default class BodyItem extends Component {
  render() {
    return (
      <div className="container ">
        <div className="row ">
          <ChildBodyItem
            icon="fa fa-address-book"
            ten="Fresh new layout"
            mota="With Bootstrap 5, weve created a fresh new layout for this template!"
          />
          <ChildBodyItem
            icon="fab fa-accessible-icon"
            ten="Free to download"
            mota="As always, Start Bootstrap has a powerful collectin of free templates."
          />
          <ChildBodyItem
            icon="fa fa-align-justify"
            ten="Jumbotron hero header"
            mota="The heroic part of this template is the jumbotron hero header!"
          />
          <ChildBodyItem
            icon="fa fa-angle-double-left"
            ten="Feature boxes"
            mota="Weve created some custom feature boxes using Bootstrap icons!"
          />
          <ChildBodyItem
            icon="fa fa-allergies"
            ten="Simple clean code"
            mota="We keep our dependencies up to date and squash bugs as they come!"
          />
          <ChildBodyItem
            icon="fab fa-adn"
            ten="A name you trust"
            mota="Start Bootstrap has been the leader in free Bootstrap templates since 2013!"
          />
        </div>
      </div>
    );
  }
}
